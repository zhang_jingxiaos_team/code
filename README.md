import pygame

import sys

import random



#加上音乐
pygame.init()

filename = 'C:\CloudMusic\music.mp3'

pygame.mixer.music.load(filename)

pygame.mixer.music.play(loops = 5, start = 0.0)


# 画面大小

ChangDu = 600

KuanDu = 600
       

# 食物

# 每个方块是一个长度，使用方法类似蛇，同样是重复进行创建，移除两个操作，只不过触发机制不同

class Food:

    def __init__(theobject):#初始块

        theobject.rect = pygame.Rect(-25,0,25,25)
        
    def set(theobject):#创建

        if theobject.rect.x == -25:

            theallobj = []

            # 创建食物时不靠墙太近，会间隔一个方块

            for pos in range(25,ChangDu-25,25):

                theallobj.append(pos)

            theobject.rect.left = random.choice(theallobj)

            theobject.rect.top  = random.choice(theallobj)

            print(theobject.rect)   

    def remove(theobject):#移除

        theobject.rect.x=-25


# 蛇类

class Snake(object):

    # 初始化各种需要的属性，开始时默认向右，长度为3方块（75）

    def __init__(theobject):

        theobject.dirction = pygame.K_RIGHT#开始时默认向右

        theobject.body = []

        for x in range(3):#长度为3方块（75）

            theobject.addnode()

        
    # 在前端增加长度，实际上就是按哪个方向键就在哪个方向加一个方块

    def addnode(theobject):#25是一个方块长度，后面的所有都遵循这个。

        left,top = (0,0)

        if theobject.body:

            left,top = (theobject.body[0].left,theobject.body[0].top)

        node = pygame.Rect(left,top,25,25)

        if theobject.dirction == pygame.K_LEFT:

            node.left -= 25

        elif theobject.dirction == pygame.K_RIGHT:

            node.left += 25

        elif theobject.dirction == pygame.K_UP:

            node.top -= 25

        elif theobject.dirction == pygame.K_DOWN:

            node.top += 25

        theobject.body.insert(0,node)#插进头部！血腥暴力！

        

    # 删除最后一个块

    def delnode(theobject):

        theobject.body.pop()

        

    # 死亡判断

    def isdead(theobject):

        if theobject.body[0].x  not in range(ChangDu):#撞墙了

            return True

        if theobject.body[0].y  not in range(KuanDu):

            return True

        if theobject.body[0] in theobject.body[1:]:#和自己相撞

            return True

        return False

        

    # 移动，实际上就是头加方块，尾减方块。冲冲冲！！！！！

    def move(theobject):

        theobject.addnode()

        theobject.delnode()

        

    # 改变方向 并且上下左右不能被逆向改变

    def changedirection(theobject,AnJian):

        LR = [pygame.K_LEFT,pygame.K_RIGHT]#对按键进行绑定

        UD = [pygame.K_UP,pygame.K_DOWN]#对按键进行绑定
      
        if AnJian in LR+UD:#如果收到正确的按键

            if (AnJian in LR) and (theobject.dirction in LR):

                return

            if (AnJian in UD) and (theobject.dirction in UD):

                return

            theobject.dirction = AnJian

    # 判断是否输入了反方向(功能冗余，留着吧，以后还可以修改)
        if AnJian == 'right' and not direction == 'left':
            
            direction = changeDirection
            
        if AnJian == 'left' and not direction == 'right':
            
            direction = changeDirection
            
        if AnJian == 'up' and not direction == 'down':
            
            direction = changeDirection
            
        if AnJian == 'down' and not direction == 'up':
            
            direction = changeDirection          


    #文字设置部分，能加的功能都加了，除了应用中文，设置之后中文会为乱码
def FenShu(screen, pos, text, color, WenZiShuXing_bold = False, WenZiSize = 60, WenZiShuXing_italic = False):

    WenZiShuXing = pygame.font.SysFont("宋体", WenZiSize)      

    WenZiGeShi = WenZiShuXing.render(text, 1, color) #填充内容 

    screen.blit(WenZiGeShi, pos)  #绘制文字 

    WenZiShuXing.set_bold(WenZiShuXing_bold)  #加粗?   

    WenZiShuXing.set_italic(WenZiShuXing_italic)    #斜体?  



     

def main():

    pygame.init()#初始化

    HuaMianDaXiao = (ChangDu,KuanDu)#画面大小，后面边框的属性注意保持一致！

    screen = pygame.display.set_mode(HuaMianDaXiao)#文字

    
    pygame.init()
    screen2 = pygame.display.set_mode((600,600),pygame.RESIZABLE,0)#边框

    pygame.display.set_caption('Snake Game')#标题

    clock = pygame.time.Clock()#改变速度用的

    scores = 0 #分数

    isdead = False #确认是否存活

    howmuch = 0 #吃了食物的数量

    # 蛇/食物

    snake = Snake()#调用函数，创建对象

    food = Food()#调用函数，创建对象
    
            
    while True:
        #通过while true 进行实时事件循环

        for event in pygame.event.get():
        #创建当前等待处理的事件的一个列表，然后使用for循环来遍历里面的事件
            
        #我们将会根据事件产生的顺序依次地进行不同的操作

            if event.type == pygame.QUIT:#使Pygame库停止工作

                sys.exit()#终止程序
                
        #添加键盘监听事件

            if event.type == pygame.KEYDOWN:

                snake.changedirection(event.key)#调用ch。angedirection函数响应按键

        # 死后按space重新开始新游戏

                if event.key == pygame.K_SPACE and isdead:

                    return main()

                

            

        screen.fill((148,0,211))#填充背景色

        

        # 画蛇 
        #每前进一步加一分

        if not isdead:

            scores+=1

            snake.move()#没死就一边动一边加分

        for rect in snake.body:#提供变色功能
            if howmuch<2:

                pygame.draw.rect(screen,(255,250,250),rect,0)#绘制矩形，也就是画蛇
                
            elif howmuch<4:
                
                pygame.draw.rect(screen,(238,232,170),rect,0)#绘制矩形，也就是画蛇
                
            elif howmuch<6:
                
                pygame.draw.rect(screen,(255,20,147),rect,0)#绘制矩形，也就是画蛇
                
            elif howmuch<8:
                
                pygame.draw.rect(screen,(75,0,130),rect,0)#绘制矩形，也就是画蛇
                
            elif howmuch<10:
                
                pygame.draw.rect(screen,(127,255,0),rect,0)#绘制矩形，也就是画蛇
                
            

        
        # 显示死亡文字

        isdead = snake.isdead()

        if isdead:

            FenShu(screen,(30,250),'~~Game Over~~',(238,232,170),False,100)

            FenShu(screen,(150,310),'pass   space   save   you!',(255,215,0),False,30)

            

        # 食物处理
        # 吃到食物+50分
        # 当食物rect与蛇头重合,吃掉。Snake增加一个方块

        if food.rect == snake.body[0]:

            scores+=50     #分数加50

            howmuch+=1     #吃的数量加一
            
            food.remove()  #移除食物

            snake.addnode()# 当食物rect与蛇头重合,吃掉。Snake增加一个方块

        

        # 当食物没了之后，新建食物，也是画方块

        food.set()

        pygame.draw.rect(screen,(136,0,21),food.rect,0)

        

        # 显示分数文字

        FenShu(screen,(10,10),'Scores: '+str(scores),(100,223,223))#分数
        
        FenShu(screen,(410,10),'eated: '+str(howmuch),(100,223,223))#吃了多少个
        

        # 边框
        pygame.draw.line(screen2,(0,191,255),(0,0),(0,600),15)#上
        
        pygame.draw.line(screen2,(0,191,255),(0,0),(600,0),15)#左

        pygame.draw.line(screen2,(0,191,255),(600,600),(0,600),15)#下

        pygame.draw.line(screen2,(0,191,255),(600,600),(600,0),15)#右

        # 网格
        A=0
        B=0
        for i in range(0,30):
            pygame.draw.line(screen2,(176,196,222),(A,0),(B,600),1)#横
            pygame.draw.line(screen2,(176,196,222),(0,A),(600,B),1)#竖
            A+=25
            B+=25
    
        pygame.display.update() #更新窗口
        
        #变速（也就是等级机制）
        if howmuch<=1:
            clock.tick(5)
        elif howmuch<=3:
            clock.tick(10)  #通过pygame 的clock函数改变速度（给我一种改变时间流速的感觉，神罗天征？简直无敌）
        elif howmuch<=5:
            clock.tick(15)
        clock.tick(20)
           

if __name__ == '__main__':

    main()
#### 介绍
{**以下是码云平台说明，您可以替换此简介**
码云是开源中国推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)